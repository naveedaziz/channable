var shopifyAPI = require('shopify-node-api');
var shopifyCred = false;
var app_id = '8464b7a6cd5d5d605672b5c61a4faed2';
var app_secret = '0f7711050792e139f5083426419b07b8';
module.exports = {
  shopify_connect: function (req,res,api_url) {
	  console.log(app_id,app_secret)
		shopifyCred = new shopifyAPI({
			  shop: api_url, // MYSHOP.myshopify.com 
			  shopify_api_key: app_id, // Your API key 
			  shopify_shared_secret: app_secret, // Your Shared Secret 
			  shopify_scope: 'write_products',
			  redirect_uri: 'http://'+req.headers.host+'/success_shopify_connect', // 'http://localhost:3000/product-listing'
			  nonce: '' // you must provide a randomly selected value unique for each authorization request 
			});
		var auth_url = shopifyCred.buildAuthURL();
		res.redirect(auth_url);
  },
  shopify_products: function(shopify_query_param){
	 // return 123;
	 return new Promise(function(success,fail){
		 shopifyCred.exchange_temporary_token(shopify_query_param, function(err, data){ //custom_collections
			shopifyCred.get('/admin/products.json?collection_id=424574856', 'collection_id=424574856,415057992', function(err, data, headers){
				  success(data);
			});
  		});
	 })
		
  },
  shopify_collections: function(shopify_query_param){
	 // return 123;
	 return new Promise(function(success,fail){
		 shopifyCred.exchange_temporary_token(shopify_query_param, function(err, data){ //custom_collections
			shopifyCred.get('/admin/custom_collections.json', '', function(err, data, headers){
				var colections = {'custom':data};
				shopifyCred.get('/admin/smart_collections.json', '', function(errs, datas, headerss){
					colections.smart = datas;
					  success(colections);
				});
			});
  		});
	 })
		
  },
  shopify_add_webhook: function (shopify_query_param) {
		var defaultParams = 	{
		  "webhook": {
			"topic": "products\/create",
			"address": "http://kfc.atequator/product_update",
			"format": "json"
		  }
		}
		 shopifyCred.exchange_temporary_token(shopify_query_param, function(err, data){
			shopifyCred.post('/admin/webhooks.json', defaultParams, function(err, data, headers){
				console.log(data)
				 return data;
			});
		});
  },
};

/*

var shopify_connect = function(req,api_url,api_key,api_secret,redirect_uri){
	redirectUri = redirect_url;
	 Shopify = new shopifyAPI({
		  shop: api_url, // MYSHOP.myshopify.com 
		  shopify_api_key: api_secret, // Your API key 
		  shopify_shared_secret: api_secret, // Your Shared Secret 
		  shopify_scope: 'write_products',
		  redirect_uri: req.headers.host+'/success_shopify_connect', // 'http://localhost:3000/product-listing'
		  nonce: '' // you must provide a randomly selected value unique for each authorization request 
		});
	var auth_url = Shopify.buildAuthURL();
	res.redirect(auth_url);
};
app.get('/success_shopify_connect',function(req,res){
	shopify_query_param  = req.query;
	res.redirect(redirectUri);
});
var shopify_products =  function(){
 	 Shopify.exchange_temporary_token(shopify_query_param, function(err, data){
		Shopify.get('/admin/products.json', 'fields=id,images,title', function(err, data, headers){
			 return data;
		});
  	});
 
};
var shopify_add_webhook =  function(){
	var defaultParams = 	{
	  "webhook": {
		"topic": "orders\/create",
		"address": "http:\/\/whatever.hostname.com\/",
		"format": "json"
	  }
	}
 	 Shopify.exchange_temporary_token(shopify_query_param, function(err, data){
		Shopify.get('/admin/webhooks.json', defaultParams, function(err, data, headers){
			 return data;
		});
  	});
 
};*/