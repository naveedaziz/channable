 var express = require("express");
 var bodyParser = require('body-parser');
 var http = require('http'),
	path = require('path');
 var Promise = require('promise');
var app = express();
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: false}));
	app.set('views', path.join(__dirname, 'public'));
	app.set('view engine', 'ejs');
	app.use(express.static(__dirname + '/public'));
var shopify = require("./shopify");
var shopify_query_param;

var google = require("./google");
 /* serves main page */
 app.get("/", function(req, res) {
    res.render('index', '');
 });
  app.get("/dashboard", function(req, res) {
    res.render('index', '');
 });
  app.get("/settings", function(req, res) {
    res.render('index', '');
 });
 app.get("/setup", function(req, res) {
    res.render('index', '');
 });
 app.get("/setup/add", function(req, res) {
    res.render('index', '');
 });
 app.get("/product", function(req, res) {
    res.render('index', '');
 });
 app.get("/channels", function(req, res) {
    res.render('index', '');
 });
 app.get("/channels/add", function(req, res) {
    res.render('index', '');
 });
  app.get("/apis", function(req, res) {
    res.render('index', '');
 });
   app.get("/apis/add", function(req, res) {
    res.render('index', '');
 });
  app.get("/adwords", function(req, res) {
    res.render('index', '');
 });
  app.get("/login", function(req, res) {
    res.render('index', '');
 });
 app.get("/signup", function(req, res) {
    res.render('index', '');
 });
  app.get("/shops", function(req, res) {
    res.render('index', '');
 });
  app.get("/shops/add", function(req, res) {
    res.render('index', '');
 });
 app.get("/shops/loading", function(req, res) {
    res.render('index', '');
 });
  app.get("/shops/add/advertiser/:id", function(req, res) {
    res.render('index', '');
 });
  app.get("/shops/add/settings/:id", function(req, res) {
    res.render('index', '');
 });
 app.get("/shops/add/collections/:id", function(req, res) {
    res.render('index', '');
 });
 
app.get("/shopify",function(req,res){
	console.log(req)
	shopify.shopify_connect(req,res,req.query.url);
})
app.get("/google",function(req,res){
	var products = google.add_product('108119358');
	products.then(function(data){
		res.send(JSON.stringify(data));
	});
})
var shopify_temp = false;
app.get('/success_shopify_connect',function(req,res){
	//console.log(req.query);
	shopify_temp = req.query;
	res.redirect('/shops/add/advertiser/facebook');
	//shopify.shopify_add_webhook(req.query)
	
});
app.get("/shopify_data",function(req,res){
	if(req.query.collection){
		var data = shopify.shopify_collections(shopify_temp);
	}
	if(req.query.product){
		var data = shopify.products(shopify_temp);
	}
	data.then(function(data){
		res.send(JSON.stringify(data));
	});
})
 /* serves all the static files */
 app.get(/^(.+)$/, function(req, res){ 
     console.log('static file request : ' + req.params);
     res.sendfile( __dirname + req.params[0]); 
 });

 var port = process.env.PORT || 3000;
 app.listen(port, function() {
   console.log("Listening on " + port);
 });