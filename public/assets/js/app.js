'use strict';
console.log(111);
// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'facebook'
]);
app.config(['$locationProvider', '$routeProvider','FacebookProvider', function($locationProvider, $routeProvider,FacebookProvider) {

     FacebookProvider.init('1822803904598090');


  $routeProvider
  .when('/', {
    templateUrl: 'views/login.html',
    controller: 'login'
  })
  .when('/dashboard', {
    templateUrl: 'views/dashboard.html',
    controller: 'dashboard'
  })
  .when('/settings', {
    templateUrl: 'views/settings.html',
    controller: 'dashboard'
  })
  .when('/setup', {
    templateUrl: 'views/setup.html',
    controller: 'dashboard'
  })
  .when('/setup/add', {
    templateUrl: 'views/setup_add.html',
    controller: 'dashboard'
  })
  .when('/product', {
    templateUrl: 'views/product.html',
    controller: 'dashboard'
  })
  .when('/channels', {
    templateUrl: 'views/channels.html',
    controller: 'dashboard'
  })
  .when('/channels/add', {
    templateUrl: 'views/channels_add.html',
    controller: 'dashboard'
  })
  .when('/apis', {
    templateUrl: 'views/apis.html',
    controller: 'dashboard'
  })
   .when('/apis/add', {
    templateUrl: 'views/apis_add.html',
    controller: 'dashboard'
  })
  .when('/adwords', {
    templateUrl: 'views/adwords.html',
    controller: 'dashboard'
  })
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'login'
  })
  .when('/signup', {
    templateUrl: 'views/signup.html',
    controller: 'login'
  })
  .when('/shops/', {
    templateUrl: 'views/shops.html',
    controller: 'shops'
  })
  .when('/shops/add/', {
    templateUrl: 'views/shops_add.html',
    controller: 'shops'
  })
  .when('/shops/loading/', {
    templateUrl: 'views/shops_loading.html',
    controller: 'shops'
  })
  .when('/shops/add/collections/:id', {
    templateUrl: 'views/shops_collections_add.html',
    controller: 'shops',
  })
  .when('/shops/add/advertiser/:id', {
    templateUrl: 'views/shops_advertiser_add.html',
    controller: 'shops',
  })
  .when('/shops/add/settings/:id', {
    templateUrl: 'views/shops_settings_add.html',
    controller: 'shops',
  })
	$locationProvider.html5Mode(true).hashPrefix('navigate');

}]);
app.controller('main', ["$rootScope",function($rootScope) {
	console.log(1111)
	$rootScope.topBar = 'views/top.html';
}]);
app.controller('dashboard', ["$rootScope",function($rootScope) {
	$rootScope.startscreens = false;
}]);
app.controller('login', ["$rootScope",function($rootScope) {
	$rootScope.startscreens = true;
}]);
app.controller('shops', ["$http","$scope","$rootScope","$routeParams","Facebook",function($http,$scope,$rootScope,$routeParams,Facebook) {
	$scope.login = function() {
      // From now on you can use the Facebook service just as Facebook api says
      Facebook.login(function(response) {
		  console.log(response)
		  $scope.me()
        // Do something with response.
      });
    };
	$scope.getLoginStatus = function() {
      Facebook.getLoginStatus(function(response) {
        if(response.status === 'connected') {
          $scope.loggedIn = true;
        } else {
          $scope.loggedIn = false;
        }
      });
    };

    $scope.me = function() {
      Facebook.api('/me', function(response) {
        $scope.user = response;
      });
    };
	
	
	
	$scope.routeParam = $routeParams.id;
	console.log($routeParams)
	$rootScope.startscreens = true;
	$scope.website = 'shopify';
	$scope.getCollection = function(){
		if($scope.website == 'shopify'){
			$http.get('/shopify_data?collection=true').then(function(data){
				$scope.allCollections = data;
			});
		}
	}
	$scope.redirectState = function(){
		if($scope.routeParam == 'facebook')
			return '/shops/add/advertiser/google';
		else if ($scope.routeParam == 'google')
			return '/shops/add/advertiser/amazon';
		else
			return '/shops/loading';
	}
}]);